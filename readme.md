# Laravel 5.3  Single File Upload and save in Database with Laravel validation and success message

 - upload single file at a time
 - valiadate the file size,type(.jpg,.pdf,.csv,.doc etc),required
 - save file in Database
 - give success message if validate passes


Project youtube tutorial link 
======

>>>
Video Tutorial for this project can be found on https://youtu.be/LVipmYUakOU  

Usage
======
    
 in **view**
 

  ```html
 {!! Form::open(array('url'=>'insertfile','method'=>'POST' ,'class'=>'form-horizontal','files'=>true)) !!}

  <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">


      <label class="control-label col-sm-2" for="name">Title:</label>
      <div class="col-sm-4">
        <input type="text" class="form-control file_title_c" id="file_title_id" name="file_title" placeholder="Enter Tile"  value="{{ Input::old('file_title') }}">

        @if ($errors->has('file_title')) <p class="help-block">{{ $errors->first('file_title') }}</p> @endif

				</div>


      </div>

      <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Upload:</label>
  
      <div class="col-sm-4">          
      
        <input type="file"  name="filenam" class="filename">

        @if ($errors->has('filenam')) <p class="help-block">{{ $errors->first('filenam') }}</p> @endif

      </div>
    </div>
    
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>



    </div>

    
{!! Form::close() !!}
  ```
  
  
```script
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

  <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

```


```script

<script>
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>
```

 in **Controller**
```laravel

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Validator;
use Session;
use Redirect;
use App\FileModel;


class UploadController extends Controller
{
    


    public function getView(){
    	return view('uploadfile');
    }

    public function insertFile(){



    	$filetitle=Input::get('file_title');
    	$file= Input::file('filenam');



    	$rules = array(
            'file_title' => 'required',
            'filenam' => 'required|max:10000|mimes:doc,docx,jpeg,png,jpg'
            ); 

    	// 'image' => 'required|mimes:jpeg,png,jpg,gif,svg,csv,xls,xlsx,doc,docx|m‌​ax:2048'



        // do the validation ----------------------------------
        // validate against the inputs from our form
        $validator = Validator::make(Input::all(), $rules);

		  if ($validator->fails()) {

            // redirect our user back with error messages       
            $messages = $validator->messages();
		    // send back to the page with the input data and errors
		    return Redirect::to('uploadfile')->withInput()->withErrors($validator);

		  }else if ($validator->passes()){

		    // checking file is valid.
		    if (Input::file('filenam')->isValid()) {

		      //$destinationPath = 'images/profile/'; // upload path
		     $extension = Input::file('filenam')->getClientOriginalExtension(); // getting image extension
		    $filename = rand(11111,99999).'.'.$extension; // renameing image


		  // uploading file to given path

		    	//$destinationPath = '../uploads';//its refers proj/uploads
                $destinationPath = 'up_file';//its refers proj/public/up_file directry


                $data=array(
                    'file_title' => $filetitle,
                    'file_name' => $filename,
                );


                FileModel::insert($data);


                $upload_success = $file->move($destinationPath, $filename);
                $notification = array(
                    'message' => 'File Uploaded successfully!', 
                    'alert-type' => 'success'
                );

                return Redirect::to('uploadfile')->with($notification);

       
		    }
		    else {
		      // sending back with error message.
		      

                $notification = array(
                        'message' => 'uploaded file is not valid!', 
                        'alert-type' => 'error'
                    );

                return Redirect::to('uploadfile')->with($notification);
		    }
  		}



    }
}
```

in **Route**
```laravel
Route::get('/uploadfile','UploadController@getView');
//--------------upload file nad store in database
Route::post('/insertfile',array('as'=>'insertfile','uses'=>'UploadController@insertFile'));
```

Important directory in this project
======
- laravel_singleFile_upload_saveInDB(projectname)
    - app
        - Http
            - controllers
                 - UploadController.php (controller)
    - routes
         -web.php   
    - resources
        - views
             -uploadfile.blade.php  (view)
    - lara_sin_file.sql (database)


